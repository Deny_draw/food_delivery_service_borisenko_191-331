function createWebsite(elements, menu, admin) {
    return new Vue({
        delimiters: ['{%', '%}'],
        el: '#website',
        data: {
            elements: elements,
            currentPage: 0,
            admAreaFilter: "",
            districtFilter: "",
            typeFilter: "",
            socialPrivilegesFilter: "",
            nameFilter: "",
            maxSeatsFilter: "",
            minSeatsFilter: "",
            isNetFilter: "",
            hasSocialPrivileges: "",
            elementsLimit: "5",
            currentCompany: null,
            menu: menu,
            amount: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            student_discount: false,
            fast_delivery: false,
            social_discount: false,
            admin: admin,
            newCompany: null
        },
        computed: {
            filteredElements: function () {
                let filtered_elements = this.elements;
                if (this.typeFilter)
                    filtered_elements = filtered_elements.filter(this.filterByType);
                if (this.admAreaFilter)
                    filtered_elements = filtered_elements.filter(this.filterByAdmArea);
                if (this.districtFilter)
                    filtered_elements = filtered_elements.filter(this.filterByDistrict);
                if (this.socialPrivilegesFilter)
                    filtered_elements = filtered_elements.filter(this.filterBySocialPrivileges);
                if (this.nameFilter)
                    filtered_elements = filtered_elements.filter(this.filterByName);
                if (this.minSeatsFilter || this.maxSeatsFilter)
                    filtered_elements = filtered_elements.filter(this.filterBySeats);
                if (this.isNetFilter)
                    filtered_elements = filtered_elements.filter(this.filterByNet);
                if (this.hasSocialPrivileges)
                    filtered_elements = filtered_elements.filter(this.filterByHasSocialPrivileges);
                return filtered_elements;
            },
            curElements: function () {
                let lastPage = Math.ceil(this.filteredElements.length / this.elementsPerPage) - 1;
                if (lastPage < 0)
                    lastPage = 0;
                if (lastPage < this.currentPage)
                    this.currentPage = lastPage;
                let offset = this.elementsPerPage * this.currentPage;
                return this.filteredElements.slice(offset, offset + this.elementsPerPage);
            },
            elementsPerPage: function () {
                return parseInt(this.elementsLimit);
            },
            orderDisabled: function(){
                for (i in this.amount) 
                    if (this.amount[i] > 0)
                        return false;
                return true;
            },
            finalSum: function(){
                let finalSum = 0;
                if (this.currentCompany){
                    for (i in this.amount)
                        finalSum += this.amount[i]*this.currentCompany["set_" + (parseInt(i) + 1)];
                    if (this.currentCompany.socialDiscount == null)
                        this.currentCompany.socialDiscount = 0;
                }
                if (this.student_discount)
                    finalSum *= 0.9;
                if (this.fast_delivery)
                    finalSum *= 1.2;
                if (this.social_discount)
                    finalSum *= ((100 - this.currentCompany.socialDiscount) / 100);
                return Math.round(finalSum);
            }
        },
        methods: {
            filterByAdmArea: function (element) {
                return element.admArea == this.admAreaFilter;
            },
            filterByDistrict: function (element) {
                return element.district == this.districtFilter;
            },
            filterByType: function (element) {
                return element.typeObject == this.typeFilter;
            },
            filterBySocialPrivileges: function (element) {
                return element.socialPrivileges == this.socialPrivilegesFilter;
            },
            filterByName: function(element) {
                return element.name.toLowerCase().indexOf(this.nameFilter.toLowerCase()) != -1;
            },
            filterBySeats: function(element) {
                if (element.seatsCount == null)
                    return false;
                let min = true;
                let max = true;
                if (this.minSeatsFilter) {
                    min = false;
                    if (parseInt(this.minSeatsFilter) <= element.seatsCount)
                        min = true;
                }
                    res = true;
                if (this.maxSeatsFilter) {
                    max = false;
                    if (parseInt(this.maxSeatsFilter) >= element.seatsCount)
                        max = true;
                }
                return min && max;
            },
            filterByNet: function(element){
                return parseInt(this.isNetFilter) == element.isNetObject;
            },
            filterByHasSocialPrivileges: function(element){
                return parseInt(this.hasSocialPrivileges) == element.socialPrivileges;
            },
            searchForOptions: function (searchField) {
                let result = [];
                let unique_elements = {};
                for (let i in this.$root.elements) 
                    if (this.$root.elements[i][searchField])
                        unique_elements[this.$root.elements[i][searchField]] = 1;
                for (let i in unique_elements)
                    result.push(i);
                result.sort();
                return result;
            },
            addNewCompany: function (){
                this.newCompany = {"id": null, "name": "", "rate": 0, "isNetObject": 0,
                                    "operatingCompany": "", "typeObject": "", "admArea": "",
                                    "district": "", "address": "", "publicPhone": "",
                                    "seatsCount": 0, "socialPrivileges": 0, "socialDiscount": 0,
                                    "created_at": null, "updated_at": null,
                                    "set_1": 1,"set_2": 2,"set_3": 3,"set_4": 4,"set_5": 5,
                                    "set_6": 6,"set_7": 7,"set_8": 8,"set_9": 9,"set_10": 10};
            },
            save_new_record: function(){
                this.elements.push(this.newCompany);
                addElement(this.newCompany);
            },
            removeRecord: function() {
                let newElements = [];
                let currentElements = this.elements;
                for (let i in currentElements) 
                    if (currentElements[i].id != this.currentCompany.id)
                        newElements.push(currentElements[i]);
                this.elements = newElements;
            },
            edit_record: function(){
                editElement(this.currentCompany);
            }
        }
    });
}

Vue.component('admarea-selector', {
    delimiters: ['{%', '%}'],
    template: `
    <div class="col-md-6">
        <h5>Административный округ</h5>
        <select class="form-control" id="type" v-bind:model="admAreaFilter" v-on:input="updateFilter($event.target.value)">
            <option value="">Не выбрано</option>
            <option v-for="tp in typeList" :value="tp">{% tp %}</option>
        </select>
    </div> `,
    computed: {
        typeList: function () {
            return this.$root.searchForOptions('admArea');
        },
        admAreaFilter: function () {
            return this.$root.admAreaFilter;
        }
    },
    methods: {
        updateFilter: function (val) {
            this.$root.admAreaFilter = val;
        }
    }
});

Vue.component('district-selector', {
    delimiters: ['{%', '%}'],
    template: `
    <div class="col-md-6 d-flex flex-column justify-content-between">
        <h5>Район</h5>
        <select class="form-control" id="type" v-bind:model="districtFilter" v-on:input="updateFilter($event.target.value)">
            <option value="">Не выбрано</option>
            <option v-for="tp in typeList" :value="tp">{% tp %}</option>
        </select>
    </div>`,
    computed: {
        typeList: function () {
            return this.$root.searchForOptions('district');
        },
        districtFilter: function () {
            return this.$root.districtFilter;
        }
    },
    methods: {
        updateFilter: function (value) {
            this.$root.districtFilter = value;
        }
    }
});

Vue.component('type-selector', {
    delimiters: ['{%', '%}'],
    template: `
    <div class="col-md-6 col-lg-6">
        <h5>Тип</h5>
        <select class="form-control" id="type" v-bind:model="typeFilter" v-on:input="updateFilter($event.target.value)">
            <option value="">Не выбрано</option>
            <option v-for="tp in typeList" :value="tp">{% tp %}</option>
        </select>
    </div>`,
    computed: {
        typeList: function () {
            return this.$root.searchForOptions('typeObject');
        },
        typeFilter: function () {
            return this.$root.typeFilter;
        }
    },
    methods: {
        updateFilter: function (value) {
            this.$root.typeFilter = value;
        }
    }
});

Vue.component('socialprivileges-selector', {
    delimiters: ['{%', '%}'],
    template: `
    <div class="col-md-6 col-lg-6">
        <h5>Соц. скидки</h5>
        <select class="form-control" id="type" v-bind:model="socialPrivilegesFilter" v-on:input="updateFilter($event.target.value)">
            <option value="">Не выбрано</option>
            <option v-for="tp in typeList" :value="tp">{% labels[tp] %}</option>
        </select>
    </div>`,
    computed: {
        typeList: function () {
            return [0, 1];
        },
        labels: function () {
            return ["Нет", "Есть"];
        },
        socialPrivilegesFilter: function () {
            return this.$root.socialPrivilegesFilter;
        }
    },
    methods: {
        updateFilter: function (val) {
            this.$root.socialPrivilegesFilter = val;
        }
    }
});

Vue.component('table-element', {
    delimiters: ['{%', '%}'],
    props: ['item'],
    template: `
    <tr>
        <td>{% item.name %}</td>
        <td>{% item.typeObject %}</td>
        <td>{% item.address %}</td>
        <td v-if="isAdmin">
            <div class="d-flex justify-content-around">
                <svg class="bi bi-eye" data-toggle="modal" data-target="#look-modal" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg" @click="setCurrent">
                    <path fill-rule="evenodd" d="M16 8s-3-5.5-8-5.5S0 8 0 8s3 5.5 8 5.5S16 8 16 8zM1.173 8a13.134 13.134 0 0 0 1.66 2.043C4.12 11.332 5.88 12.5 8 12.5c2.12 0 3.879-1.168 5.168-2.457A13.134 13.134 0 0 0 14.828 8a13.133 13.133 0 0 0-1.66-2.043C11.879 4.668 10.119 3.5 8 3.5c-2.12 0-3.879 1.168-5.168 2.457A13.133 13.133 0 0 0 1.172 8z"/>
                    <path fill-rule="evenodd" d="M8 5.5a2.5 2.5 0 1 0 0 5 2.5 2.5 0 0 0 0-5zM4.5 8a3.5 3.5 0 1 1 7 0 3.5 3.5 0 0 1-7 0z"/>
                </svg>
                <svg class="bi bi-pencil" data-toggle="modal" data-target="#edit-modal" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg" @click="setCurrent">
                    <path fill-rule="evenodd" d="M11.293 1.293a1 1 0 0 1 1.414 0l2 2a1 1 0 0 1 0 1.414l-9 9a1 1 0 0 1-.39.242l-3 1a1 1 0 0 1-1.266-1.265l1-3a1 1 0 0 1 .242-.391l9-9zM12 2l2 2-9 9-3 1 1-3 9-9z"/>
                    <path fill-rule="evenodd" d="M12.146 6.354l-2.5-2.5.708-.708 2.5 2.5-.707.708zM3 10v.5a.5.5 0 0 0 .5.5H4v.5a.5.5 0 0 0 .5.5H5v.5a.5.5 0 0 0 .5.5H6v-1.5a.5.5 0 0 0-.5-.5H5v-.5a.5.5 0 0 0-.5-.5H3z"/>
                </svg>
                <svg class="bi bi-trash" data-toggle="modal" data-target="#delete-modal" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg" @click="setCurrent">
                    <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"/>
                    <path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4L4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"/>
                </svg>
            </div>
        </td>
        <td v-else><button type="button" class="btn btn-success" :id="'search-' + this.item.id" @click="setCurrent">Выбрать</button></td>
    </tr>`,
    computed: {
        isAdmin: function() {
            return this.$root.admin;
        }
    },
    methods: {
        setCurrent: function () {
            if(this.$root.currentCompany != this.item)
            {
                let newAmount = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
                this.$root.amount = newAmount;
                this.$root.currentCompany = this.item;
            }
        }
    }
});

Vue.component('all-table-elements', {
    delimiters: ['{%', '%}'],
    template: `
    <table class="table table-striped">
        <thead>
            <tr>
                <th>Название</th>
                <th>Тип</th>
                <th>Адрес</th>
                <th>Выбрать</th>
            </tr>
        </thead>
        <tbody id="table">
            <template v-for="item in curElements">
                <table-element :item="item"></table-element>
            </template>
        </tbody>
    </table>`,
    computed: {
        curElements: function () {
            return this.$root.curElements;
        }
    }
});

Vue.component("pagination", {
    delimiters: ['{%', '%}'],
    template: `
    <nav aria-label="...">
        <ul class="pagination">
            <li class="page-item" v-if="currentPage > 0">
                <a class="page-link" tabindex="-1" @click="return toPage(0)">На первую</a>
            </li>
            <li class="page-item" v-if="currentPage > 0"><a @click="return toFirst()" class="page-link">{% displayPages.prev %}</a></li>
            <li class="page-item active" aria-current="page">
                <a class="page-link"><span>{% displayPages.curr %}</span></a>
            </li>
            <li class="page-item" v-if="currentPage < lastPages - 1"><a @click="return toLast()" class="page-link">{% displayPages.next %}</a></li>
            <li class="page-item" v-if="currentPage < lastPages - 1">
                <a class="page-link" @click="return toPage(lastPages - 1)">На последнюю</a>
            </li>
        </ul>
    </nav>`,
    computed: {
        currentPage: function () {
            return this.$root.currentPage;
        },
        lastPages: function () {
            let l = this.$root.filteredElements.length;
            return Math.ceil(l / this.$root.elementsPerPage);
        },
        displayPages: function () {
            return { 'prev': this.currentPage, 'curr': this.currentPage + 1, 'next': this.currentPage + 2 };
        }
    },
    methods: {
        toFirst: function () {
            this.$root.currentPage--;
            return false;
        },
        toLast: function () {
            this.$root.currentPage++;
            return false;
        },
        toPage: function (num) {
            this.$root.currentPage = num;
            return false;
        }
    }
});

Vue.component('menu-item-cell', {
    delimiters: ['{%', '%}'],
    props: ["menuItem", "index"],
    template: `
    <div class="col-sm-6 col-lg-4">
        <div class="card menu-card mb-3 text-center" style="width: 18rem;" >
            <img :src="menuItem.img" class="card-img-top">
            <div class="card-body">
                <h5 class="card-title">{% menuItem.name %}</h5>
                <p class="card-text">{% menuItem.description %}</p>
                <h5>{% currentPrice %}&nbsp;₽</h5>
            </div>
            <div class="price">
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <button class="btn btn-outline-secondary" :disabled="currentAmount == 0" type="button" @click = "decAmount()">-</button>
                    </div>
                    <input class="form-control amount" min="0" :value="currentAmount" type="number" id="value" placeholder="0" v-bind:model="currentAmount" v-on:input="updateAmount($event.target.value)">
                    <div class="input-group-append">
                        <button class="btn btn-outline-secondary" type="button" @click="incAmount()">+</button>
                    </div>
                </div>
            </div>
        </div>
    </div>`,
    computed: {
        pos: function () {
            return parseInt(this.index) - 1;
        },
        currentPrice: function () {
            return this.$root.currentCompany["set_" + this.index]
        },
        currentAmount: function () {
            return this.$root.amount[this.pos];
        }
    },
    methods: {
        updateAmount: function (amount) {
            let newAmount = [...this.$root.amount];
            newAmount[this.pos] = amount;
            this.$root.amount = newAmount;
        },
        incAmount: function () {
            let newAmount = [...this.$root.amount];
            newAmount[this.pos]++;
            this.$root.amount = newAmount;
        },
        decAmount: function () {
            let newAmount = [...this.$root.amount];
            newAmount[this.pos]--;
            this.$root.amount = newAmount;
        }
    }
});

Vue.component('menu-cell', {
    delimiters: ['{%', '%}'],
    template: `
    <div class="mb-5">
        <h2 class="mb-4 text-center">Меню</h2>
        <div class="row">
            <menu-item-cell v-for = "(menuItem, id) in menu" :menuItem = "menuItem" :index = "id + 1" ></menu-item-cell>
        </div>
    </div>`,
    computed: {
        menu: function () {
            return this.$root.menu;
        }
    }
});

Vue.component('order-position',{
    delimiters: ['{%', '%}'],
    props: ['menuitem'],
    template: `
    <div class="row mt-4 text-center d-flex align-items-center pt-2 pb-2" style="border: 1px solid lightgray; border-radius: 5px;">
        <div class="col-lg-2">
            <img :src="pos.img" width="100%">
        </div>
        <div class="col-lg-4 text-left text-center">
            <span class="order-item-name">{% pos.name %}</span>
        </div>
        <div class="col-lg-3">
            <div class="text-center">
                <span>{% amount %}</span>
                <span>x</span>
                <span>{% price %}</span>
                <span>Р</span>
            </div>
        </div>
        <div class="col-lg-3 text-right text-center">
            <b>
                <span>{% total %}</span>
                <span>Р</span>
            </b>
        </div>
    </div>`,
    computed: {
        pos: function(){
            return this.$root.menu[parseInt(this.menuitem)];
        },
        price: function() {
            return this.$root.currentCompany['set_' + (parseInt(this.menuitem) + 1)];
        },
        amount: function() {
            return this.$root.amount[parseInt(this.menuitem)];
        },
        total: function() {
            return this.price * this.amount;
        }
    }
});

Vue.component('order-item',{
    delimiters: ['{%', '%}'],
    template: `
    <div class="modal-body container">
        <h4>Позиции заказа</h4>
        <order-position v-for="pos in positions" :menuitem="pos"></order-position>
        <div class="mt-4" v-if="options_present">
            <h4>Дополнительные опции</h4>
            <div class="d-flex justify-content-md-between align-items-center active-checkbox" v-if="student_discount">
                <div>
                    <span>Cтудент ВУЗа:</span>
                </div>
                <div class="text-md-right">
                    <b>
                        <span>-</span>
                        <span>10</span>
                        <span>%</span>
                    </b>
                </div>
            </div>
            <div class="d-flex mt-2 justify-content-md-between align-items-center active-checkbox" v-if="fast_delivery">
                <div>
                    <span>Быстрая доставка:</span>
                </div>
                <div class="text-md-right">
                    <b>
                        <span>+</span>
                        <span>20</span>
                        <span>%</span>
                    </b>
                </div>
            </div>
            <div class="d-flex justify-content-md-between align-items-center active-checkbox" v-if="social_discount">
                <div>
                    <span>Социальные скидки предприятия:</span>
                </div>
                <div class="text-md-right">
                    <b>
                        <span>-</span>
                        <span>{% currentCompany.socialDiscount %}</span>
                        <span>%</span>
                    </b>
                </div>
            </div>
        </div>
        <div class="mt-4">
            <h4>Информация о предприятии</h4>
            <div class="row">
                <div class="col-md-5 col-sm-12">
                    <span><b>Название</b></span>
                </div>
                <div class="col-md-7 col-sm-12">
                    <span>{% currentCompany.name %}</span>
                </div>
            </div>
            <div class="row mt-2">
                <div  class="col-md-5 col-sm-12">
                    <span><b>Административный округ</b></span>
                </div>
                <div class="col-md-7 col-sm-12">
                    <span>{% currentCompany.admArea %}</span>
                </div>
            </div>
            <div class="row mt-2">
                <div class="col-md-5 col-sm-12">
                    <span><b>Район</b></span>
                </div>
                <div class="col-md-7 col-sm-12">
                    <span>{% currentCompany.district %}</span>
                </div>
            </div>
            <div class="row mt-2">
                <div class="col-md-5 col-sm-12">
                    <span><b>Адрес</b></span>
                </div>
                <div class="col-md-7 col-sm-12">
                    <span>{% currentCompany.address %}</span>
                </div>
            </div>
            <div class="row mt-2">
                <div class="col-md-5 col-sm-12">
                    <span><b>Рейтинг</b></span>
                </div>
                <div class="col-md-7 col-sm-12">
                    <span>{% currentCompany.rate %}</span>
                </div>
            </div>
        </div>
        <div class="mt-4">
            <h4>Доставка</h4>
            <form class="form-group d-md-flex flex-column mt-2 align-items-center">
                <div class="row mt-2">
                    <div class="col-md-6"><span>Зона доставки:</span></div>
                    <div class="col-md-6">
                        <select class="custom-select" id="inputGroupSelect01">
                            <option>Не выбрано</option>
                            <option>Первая зона</option>
                            <option>Вторая зона</option>
                            <option>Третья зона</option>
                        </select>
                    </div>
                </div>
                
                <div class="row mt-2">
                    <div class="col-md-6"><span>Адрес доставки:</span></div>
                    <div class="col-md-6">
                        <textarea  class="form-control" aria-label="With textarea"></textarea>
                    </div>
                </div>
                
                <div class="row mt-2">
                    <div class="col-md-6">
                        <span>Стоимость доставки:</span>
                    </div>
                    <div class="col-md-6">
                        <span>250</span>
                        <span>Р</span>
                    </div>
                </div>
                
                <div class="row mt-2">
                    <div class="col-md-6">
                        <span>ФИО получателя:</span>
                    </div>
                    <div class="col-md-6">
                        <input  type="text" class="form-control" aria-label="Username" aria-describedby="basic-addon1" placeholder="Фамилия Имя Отчество">
                    </div>
                </div>
            </form>
            
            <div class="d-md-flex mt-2 justify-content-md-between align-items-center">
                <div class="row" style="align-items: center;">
                    <div class="col-md-6"><label class="col-form-label col-form-label-lg"><h4>Итого:</h4></label></div>
                    <div class="col-md-6">
                        <h4>
                            <span>{% finalSum %}</span>
                            <span>₽</span>
                        </h4>
                    </div>
                </div>
            </div> 
        </div>
    </div>`,
    computed: {
        currentCompany: function() {
            return this.$root.currentCompany;
        },
        positions: function() {
            let res = [];
            for (i in this.$root.amount)
                if (this.$root.amount[i] > 0)
                    res.push(i);
            return res;
        },
        finalSum: function(){
            return this.$root.finalSum + 250;
        },
        student_discount: function(){
            return this.$root.student_discount;
        },
        fast_delivery: function(){
            return this.$root.fast_delivery;
        },
        social_discount: function(){
            return this.$root.social_discount;
        },
        options_present: function(){
            return this.$root.fast_delivery || this.$root.student_discount || this.$root.social_discount;
        }
    }
    
});

Vue.component('look-modal', {
    delimiters: ['{%', '%}'],
    template:`
    <div class="modal-content">
        <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel">{% currentCompany.name %}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body row">
            <div class="col-md-6 col-sm-12"><h5>Наименование:</h5></div>
            <div class="col-md-6 col-sm-12">{% currentCompany.name %}</div>
            <div class="col-md-6 col-sm-12"><h5>Является сетевым:</h5></div>
            <div class="col-md-6 col-sm-12">{% currentCompany.isNetObject | boolNetObjectToString %}</div>
            <div class="col-md-6 col-sm-12"><h5>Название управляющей компании:</h5></div>
            <div class="col-md-6 col-sm-12">{% currentCompany.operatingCompany %}</div>
            <div class="col-md-6 col-sm-12"><h5>Вид объекта:</h5></div>
            <div class="col-md-6 col-sm-12">{% currentCompany.typeObject %}</div>
            <div class="col-md-6 col-sm-12"><h5>Административный округ:</h5></div>
            <div class="col-md-6 col-sm-12">{% currentCompany.admArea %}</div>
            <div class="col-md-6 col-sm-12"><h5>Район:</h5></div>
            <div class="col-md-6 col-sm-12">{% currentCompany.district %}</div>
            <div class="col-md-6 col-sm-12"><h5>Адрес:</h5></div>
            <div class="col-md-6 col-sm-12">{% currentCompany.address %}</div>
            <div class="col-md-6 col-sm-12"><h5>Число посадочных мест:</h5></div>
            <div class="col-md-6 col-sm-12">{% currentCompany.seatsCount %}</div>
            <div class="col-md-6 col-sm-12"><h5>Показатель социальных льгот:</h5></div>
            <div class="col-md-6 col-sm-12">{% currentCompany.socialPrivileges | boolSocialPrivilegesToString %}</div>
            <div class="col-md-6 col-sm-12"><h5>Контактный телефон:</h5></div>
            <div class="col-md-6 col-sm-12">{% currentCompany.publicPhone %}</div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-primary confirm-order">Ок</button>
        </div>
    </div>`,
    filters: {
        boolNetObjectToString: function (value) {
            if (!value) return "Нет";
            return "Да";
        },
        boolSocialPrivilegesToString: function (value) {
            if (!value) return "Нет";
            return "Есть";
        }
    },
    computed: {
        currentCompany: function() {
            return this.$root.currentCompany;
        }
    }
});

Vue.component('edit-modal',{
    delimiters: ['{%', '%}'],
    template: `
    <div class="order-form container">
        <div class="modal-body container">
            <form class="form-group d-md-flex flex-column mt-2">
                <div>
                    <span>Наименование</span>
                    <input type="text" class="form-control" @blur="setValue($event.target.value, 'name')" :value="currentCompany.name" aria-label="Username" aria-describedby="basic-addon1">
                </div>
                
                <div class="mt-3">
                    <span>Является сетевым</span>
                    <div class="d-flex flex-row">
                        <div class="form-check pr-4">
                            <input class="form-check-input" :checked="currentCompany.isNetObject == 1" v-on:input="setValue($event.target.value, 'isNetObject')" type="radio" name="isNetObject" id="exampleRadios2" value="1">
                            <label class="form-check-label" for="exampleRadios2">Да</label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" :checked="currentCompany.isNetObject == 0" v-on:input="setValue($event.target.value, 'isNetObject')" type="radio" name="isNetObject" id="exampleRadios2" value="0">
                            <label class="form-check-label" for="exampleRadios2">Нет</label>
                        </div>
                    </div>
                </div>

                <div class="mt-3">
                    <span>Название управляющей компании</span>
                    <input  type="text" class="form-control" @blur="setValue($event.target.value, 'operatingCompany')" :value="currentCompany.operatingCompany" aria-label="Username" aria-describedby="basic-addon1">
                </div>
                
                <div class="mt-3">
                    <span>Вид объекта</span>
                    <select class="custom-select" id="inputGroupSelect01" @input="setValue($event.target.value, 'typeObject')">
                        <option v-for="tp in typeList('typeObject')" :value="tp" :selected="tp == currentCompany.typeObject">{% tp %}</option>
                    </select>
                </div>

                <div class="mt-3">
                    <span>Административный округ</span>
                    <select class="custom-select" id="inputGroupSelect01" @input="setValue($event.target.value, 'admArea')">
                        <option v-for="tp in typeList('admArea')" :value="tp" :selected="tp == currentCompany.admArea">{% tp %}</option>
                    </select>
                </div>

                <div class="mt-3">
                    <span>Район</span>
                    <select class="custom-select" id="inputGroupSelect01" @input="setValue($event.target.value, 'district')">
                        <option v-for="tp in typeList('district')" :value="tp" :selected="tp == currentCompany.district">{% tp %}</option>
                    </select>
                </div>
                
                <div class="mt-3">
                    <span>Адрес</span>
                    <textarea  class="form-control" @blur="setValue($event.target.value, 'address')" :value="currentCompany.address" aria-label="With textarea"></textarea>
                </div>

                <div class="mt-3">
                    <span>Число посадочных мест</span>
                    <input  type="text" class="form-control" @blur="setValue($event.target.value, 'seatsCount')" :value="currentCompany.seatsCount" aria-label="Username" aria-describedby="basic-addon1">
                </div>
                
                <div class="mt-3">
                    <span>Показатель социальных льгот</span>
                    <div class="d-flex flex-row">
                        <div class="form-check pr-4">
                            <input class="form-check-input" :checked="currentCompany.socialPrivileges == 1" v-on:input="setValue($event.target.value, 'socialPrivileges')" type="radio" name="socialPrivileges" id="exampleRadios2" value="1">
                            <label class="form-check-label" for="exampleRadios2">Есть</label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" :checked="currentCompany.socialPrivileges == 0" v-on:input="setValue($event.target.value, 'socialPrivileges')" type="radio" name="socialPrivileges" id="exampleRadios2" value="0">
                            <label class="form-check-label" for="exampleRadios2">Нет</label>
                        </div>
                    </div>
                </div>

                <div class="mt-3">
                    <span>Телефон</span>
                    <input  type="text" class="form-control" @blur="setValue($event.target.value, 'publicPhone')" :value="currentCompany.publicPhone" aria-label="Username" aria-describedby="basic-addon1">
                </div>
            </form>
        </div>
    </div>`,
    computed: {
        currentCompany: function(){
            return this.$root.currentCompany;
        }
    },
    methods:{
        typeList: function (field) {
            return this.$root.searchForOptions(field);
        },
        setValue: function(value, name){
            this.$root.currentCompany[name] = value;
        },
    }
});

Vue.component('delete-record-modal',{
    delimiters: ['{%', '%}'],
    template:`
    <div class="modal-content">
        <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel">Удаление записи</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body">
            Вы уверены, что хотите удалить данные предприятия {% currentCompany.name %}?
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Нет</button>
            <button type="button" class="btn btn-primary confirm-order"  data-dismiss="modal" @click="delete_record">Да</button>
        </div>
    </div>`,
    computed: {
        currentCompany: function(){
            return this.$root.currentCompany;
        }
    },
    methods: {
        delete_record: function(){
            this.$root.removeRecord();
            deleteElement(this.currentCompany.id);
        }
    }
});

Vue.component('add-modal',{
    delimiters: ['{%', '%}'],
    template: `
    <div class="order-form container">
        <div class="modal-body container">
            <form class="form-group d-md-flex flex-column mt-2">
                <div>
                    <span>Наименование</span>
                    <input  type="text" class="form-control" @blur="setValue($event.target.value, 'name')" aria-label="Username" @blur="setValue($event.target.value, 'name')" aria-describedby="basic-addon1">
                </div>
                
                <div class="mt-3">
                    <span>Является сетевым</span>
                    <div class="d-flex flex-row">
                        <div class="form-check pr-4">
                            <input class="form-check-input" @input="setValue($event.target.value, 'isNetObject')" type="radio" name="isNetObject" id="isNetObject" value="1">
                            <label class="form-check-label" for="isNetObject">Да</label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" @input="setValue($event.target.value, 'isNetObject')" type="radio" name="isNetObject" id="isNetObject" value="0">
                            <label class="form-check-label" for="isNetObject">Нет</label>
                        </div>
                    </div>
                </div>

                <div class="mt-3">
                    <span>Название управляющей компании</span>
                    <input  type="text" @blur="setValue($event.target.value, 'operatingCompany')" class="form-control" aria-label="Username" aria-describedby="basic-addon1">
                </div>
                
                <div class="mt-3">
                    <span>Вид объекта</span>
                    <select class="custom-select" @input="setValue($event.target.value, 'typeObject')" id="inputGroupSelect01">
                        <option v-for="tp in typeList('typeObject')" :value="tp" :selected="tp == 'Не выбрано'">{% tp %}</option>
                    </select>
                </div>

                <div class="mt-3">
                    <span>Административный округ</span>
                    <select class="custom-select" @input="setValue($event.target.value, 'admArea')" id="inputGroupSelect01">
                        <option v-for="tp in typeList('admArea')" :value="tp" :selected="tp == 'Не выбрано'">{% tp %}</option>
                    </select>
                </div>

                <div class="mt-3">
                    <span>Район</span>
                    <select class="custom-select" @input="setValue($event.target.value, 'district')" id="inputGroupSelect01">
                        <option v-for="tp in typeList('district')" :value="tp" :selected="tp == 'Не выбрано'">{% tp %}</option>
                    </select>
                </div>

                <div class="mt-3">
                    <span>Адрес</span>
                    <textarea @blur="setValue($event.target.value, 'address')" class="form-control" aria-label="With textarea"></textarea>
                </div>

                <div class="mt-3">
                    <span>Число посадочных мест</span>
                    <input @blur="setValue($event.target.value, 'seatsCount')" type="text" class="form-control" aria-label="Username" aria-describedby="basic-addon1">
                </div>
                
                <div class="mt-3">
                    <span>Показатель социальных льгот</span>
                    <div class="d-flex flex-row">
                        <div class="form-check pr-4">
                            <input class="form-check-input" @input="setValue($event.target.value, 'socialPrivileges')" type="radio" name="hasSocialPrivileges" id="hasSocialPrivileges" value="1">
                            <label class="form-check-label" for="hasSocialPrivileges">Есть</label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" @input="setValue($event.target.value, 'socialPrivileges')" type="radio" name="hasSocialPrivileges" id="hasSocialPrivileges" value="0">
                            <label class="form-check-label" for="hasSocialPrivileges">Нет</label>
                        </div>
                    </div>
                </div>

                <div class="mt-3">
                    <span>Телефон</span>
                    <input @blur="setValue($event.target.value, 'publicPhone')" type="text" class="form-control" aria-label="Username" aria-describedby="basic-addon1">
                </div>
            </form>
        </div>
    </div> `,
    computed: {
        currentCompany: function(){
            return this.$root.newCompany;
        }
    },
    methods:{
        typeList: function (field) {
            return this.$root.searchForOptions(field);
        },
        setValue: function(value, name){
            this.$root.newCompany[name] = value;
        }
    }
});

const web = function (elements, menu) {
    return createWebsite(elements, menu, isAdmin);
}

$(document).ready(function () {
    jQuery.get("/menu_dishes.json", {}, function (data) {
        menu = data;
        jQuery.ajax({
            url: 'http://exam-2020-1-api.std-400.ist.mospolytech.ru/api/data1',
            type: 'GET',
            crossDomain: true,
            success: function (data) {
                $('.text-success').hide();
                $(`#displayed-num-of-records`).show();
                $(`#search-form`).show();
                $('loading-admin-filters').hide();
                web(data, menu);
            }
        });
    })
});

function addElement(element) {
    jQuery.ajax({
        url: 'http://exam-2020-1-api.std-400.ist.mospolytech.ru/api/data1',
        type: 'POST',
        crossDomain: true,
        data: element,
        success: function(data) {
            element.id = data.id;
            element.created_at = data.created_at;
            element.updated_at = data.updated_at;
        },
    });
}
  
function editElement(element) {
    jQuery.ajax({
        url: 'http://exam-2020-1-api.std-400.ist.mospolytech.ru/api/data1/' + element.id,
        type: 'PUT',
        crossDomain: true,
        data: element
    });
}
  
function deleteElement(id) {
    jQuery.ajax({
        url: 'http://exam-2020-1-api.std-400.ist.mospolytech.ru/api/data1/' + id,
        type: 'DELETE',
        crossDomain: true
    });
}

let menu;